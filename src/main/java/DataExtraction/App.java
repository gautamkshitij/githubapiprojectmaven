package DataExtraction;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import Project.Comment;
import Project.GithubProject;
import Project.Issue;
import WriteToFile.WritetoFile;

public class App {
	// static String accessToken = "";

	static String accessToken = "&access_token=d7d11706ca48e3f40327dbc7490c03d600d7f614";

	static int count = 1;

	/**
	 * @param args
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws IOException
	 * @throws UnsupportedOperationException
	 */
	public static void main(String[] args) throws IOException {
		String apiURL = "";
		List<String> dates = getDatesAccordingWeeks();

		WritetoFile.initializeWriting();

		if (dates.size() != 0) {

			for (int i = 0; i < dates.size() - 1; i++) {

				apiURL = "https://api.github.com/search/repositories?q=NOT+android+NOT+javascript+NOT+jquery+language:java+created:"
						+ dates.get(i) + ".." + dates.get(i + 1) + "&sort=stars&order=desc" + accessToken;
				startDownload(apiURL);
			}

		}

		WritetoFile.finalizeWriting();

	}

	public static void startDownload(String apiURL)
			throws JsonIOException, JsonSyntaxException, UnsupportedOperationException, IOException {
		GithubProject project = null;
		JsonParser parser = new JsonParser();
		String issueURL = "";

		int lastPage = ReadJSONfromURL.getLastPage(apiURL);
		for (int j = 1; j <= lastPage; j++) {

			Object searchAPIResult = parser.parse(ReadJSONfromURL.getJson(apiURL + "&page=" + j));

			System.out.println(apiURL + "&page=" + j);

			JsonObject searchResults = (JsonObject) searchAPIResult;
			if (searchResults.isJsonObject()) {

				JsonArray items = (JsonArray) searchResults.get("items");

				if (items.isJsonArray()) {

					Iterator<JsonElement> itemIterator = items.iterator();

					while (itemIterator.hasNext()) {
						System.out.println("Project: " + count++);

						JsonObject projectObject = (JsonObject) itemIterator.next();

						if (projectObject.get("has_issues").toString().equalsIgnoreCase("true")) {
							project = new GithubProject();
							project.setId(projectObject.get("id").getAsLong());
							project.setCloneURL(projectObject.get("clone_url").toString().replaceAll("\"", ""));
							project.setStars(projectObject.get("stargazers_count").getAsLong());
							project.setDescription(projectObject.get("description").toString().replaceAll("\"", ""));
							project.setHomepage(projectObject.get("homepage").toString().replaceAll("\"", ""));
							project.setProjectName(projectObject.get("name").toString().replaceAll("\"", ""));
							project.setFinalCloneURL("git clone " + project.getCloneURL().replaceAll("\"", "")
									+ " ~/Github/JAVA/" + project.getProjectName() + "/");

							/*
							 * get number of pages of issues in this project
							 * from Headers and then iterate over them
							 */

							issueURL = projectObject.get("issues_url").toString().replace("{/number}", "")
									.replaceAll("\"", "") + "?per_page=100" + accessToken;

							// System.out.println(issueURL);

							List<Issue> issuesOfProject = new ArrayList<Issue>();
							int lastpageIssue = ReadJSONfromURL.getLastPage(issueURL);

							// System.out.println("lastpage: " + lastpageIssue);

							for (int i = 1; i <= lastpageIssue; i++) {
								// System.out.println("HELLLOO");

								Object issues = parser.parse(ReadJSONfromURL.getJson(issueURL + "&page=" + i).trim());

								if (issues != null) {

									try {
										JsonArray issuesProject = (JsonArray) issues;
										if (issuesProject.isJsonArray()) {

											// System.out.println("IssueProject"
											// + issuesProject);
											Iterator<JsonElement> issueIterator = issuesProject.iterator();
											Issue issue;

											while (issueIterator.hasNext()) {
												JsonObject issueObject = (JsonObject) issueIterator.next();

												issue = new Issue();
												issue.setBody(issueObject.get("body").toString().replaceAll("\"", ""));
												issue.setTitle(
														issueObject.get("title").toString().replaceAll("\"", ""));
												issue.setId(issueObject.get("id").getAsLong());
												issue.setState(
														issueObject.get("state").toString().replaceAll("\"", ""));
												issue.setUrl(issueObject.get("url").toString().replaceAll("\"", ""));
												JsonObject pullRequest = (JsonObject) issueObject.get("pull_request");
												if (pullRequest != null) {

													issue.setDiff(pullRequest.get("diff_url").toString()
															.replaceAll("\"", ""));
													issue.setPatch(pullRequest.get("patch_url").toString()
															.replaceAll("\"", ""));

												} else {

													issue.setDiff("");
													issue.setPatch("");
												}

												/*
												 * loading comments of an Issue
												 */

												int numberOfComments = issueObject.get("comments").getAsInt();

												if (numberOfComments > 0) {

													String commentsURL = issueObject.get("comments_url").getAsString();

													if (commentsURL.length() != 0) {

														commentsURL = commentsURL.trim() + "?" + accessToken
																+ "&per_page=100";
														// System.out.println("CommentsURL
														// " + commentsURL);

														Object comments = ReadJSONfromURL.getJson(commentsURL);

														// System.out.println(comments);

														JsonArray commentsOfIssue = null;
														try {
															commentsOfIssue = (JsonArray) comments;

															// System.out.println(commentsOfIssue);
														} catch (Exception e) {
														}
														if (commentsOfIssue != null && commentsOfIssue.isJsonArray()) {
															Comment comment;
															List<Comment> commentsIssue = new ArrayList<Comment>();

															Iterator<JsonElement> commentsIterator = commentsOfIssue
																	.iterator();

															while (commentsIterator.hasNext()) {
																JsonObject commentJSON = (JsonObject) commentsIterator
																		.next();
																JsonObject userObject = (JsonObject) commentJSON
																		.get("user");

																commentsIssue.add(
																		new Comment(commentJSON.get("id").getAsLong(),
																				userObject.get("id").getAsLong(),
																				commentJSON.get("created_at")
																						.getAsString(),
																		commentJSON.get("body").getAsString()));

															}
															issue.setComments(commentsIssue);
														}
													}
												} else {
													issue.setComments(null);
												}

												issuesOfProject.add(issue);

											}
											project.setIsssues(issuesOfProject);
										}
									} catch (Exception e) {
										System.out.println(e + "==== Issues Object not array" + issues);
									}

								}

							}

							WritetoFile.writeProjectToFile(project);
							// System.out.println(project);
						}

					}
				}
			}
		}

	}

	/*
	 * Gives range of dates from 2007-01-01 to 2015-12-01
	 */
	public static List<String> getDatesAccordingMonths() {
		List<String> dates = new ArrayList<String>();
		try {
			Calendar gcal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start = sdf.parse("2008-01-01");
			Date end = sdf.parse("2015-12-01");
			gcal.setTime(start);
			do {
				Date d = gcal.getTime();
				dates.add(sdf.format(d));
				gcal.add(Calendar.MONTH, 1);
			} while (gcal.getTime().before(end));
		} catch (ParseException exp) {
			exp.printStackTrace();
		}

		return dates;
	}

	/*
	 * Gives range of dates from 2008-01-01 to 2015-12-01
	 * 
	 * Phase1: 2012-11-02 to 2014-07-31, Server101 Phase2: 2014-08-01 to
	 * 2015-11-18 (server 102)
	 * 
	 */
	public static List<String> getDatesAccordingWeeks() {
		List<String> dates = new ArrayList<String>();
		try {
			Calendar gcal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start = sdf.parse("2012-11-02");
			Date end = sdf.parse("2014-07-31");
			gcal.setTime(start);
			do {
				Date d = gcal.getTime();

				dates.add(sdf.format(d));
				gcal.add(Calendar.DATE, 7);

			} while (gcal.getTime().before(end));
		} catch (ParseException exp) {
			exp.printStackTrace();
		}

		return dates;
	}

	/*
	 * 1.
	 * https://api.github.com/search/repositories?language:java&sort=stars&order
	 * =desc 2.
	 * https://api.github.com/search/repositories?q=NOT+android+NOT+javascript+
	 * NOT+jquery+language:java&sort=stars&order=desc
	 */

	/*
	 * kshitij's access TOken: 860cbc060b9d69a02b63814d3e5b89549bd1ad94 Shrey's
	 * Access Token: d7d11706ca48e3f40327dbc7490c03d600d7f614
	 */
}
