package DataExtraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class ReadJSONfromURL {
	// String url =
	// "https://api.github.com/search/repositories?q=language:java&sort=stars&order=desc&access_token=637e8e922f37bc7463405e7dbbeceb0f2ac191d4";

	private static DefaultHttpClient client, client2;

	@SuppressWarnings("deprecation")

	public static String getJson(String apiURL) throws UnsupportedOperationException, IOException {
		int remaining = -1;
		long resettime = -1;

		StringBuffer result = null;
		client = new DefaultHttpClient();

		HttpGet request = new HttpGet(apiURL);

		request.addHeader("User-Agent", "Shrey_Help");
		HttpResponse response = null;
		do {
			if (response != null) {
				EntityUtils.consume(response.getEntity());
			}
			response = client.execute(request);
			Header[] headers = response.getAllHeaders();

			for (Header header : headers) {
				if (header.getName().equalsIgnoreCase("X-RateLimit-Remaining")) {
					remaining = Integer.parseInt((header.getValue()));
				}
				if (header.getName().equalsIgnoreCase("X-RateLimit-Reset")) {
					resettime = Long.parseLong((header.getValue()));
				}
				if (header.getName().equalsIgnoreCase("X-RateLimit-Reset")) {
					resettime = Long.parseLong((header.getValue()));
				}

			}
			System.out.println("Status Code: " + response.getStatusLine().getStatusCode());
			if (remaining == 0) {
				/*
				 * You need to consume the response body before you can reuse
				 * the connection for another request. You should not only read
				 * the response status, but read the response InputStream fully
				 * to the last byte whereby you just ignore the read bytes.
				 */

				long sleepTimeMiliseconds = (resettime * 1000 - System.currentTimeMillis());

				if (sleepTimeMiliseconds > 0) {
					try {
						System.out.println("CurrentTime: " + System.currentTimeMillis() + " --Sleeping for: "
								+ (sleepTimeMiliseconds + 1000) / 1000 + "seconds --" + "starting again: "
								+ resettime * 1000);

						TimeUnit.MILLISECONDS.sleep(sleepTimeMiliseconds + 1000);

					} catch (InterruptedException e) {
						System.err.println(e + "--------- Sleep Interrupted");
					}

				}

			}

		} while (response.getStatusLine().getStatusCode() != 200);

		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();

	}

	public static int getLastPage(String apiURL2) throws ClientProtocolException, IOException {
		int remaining1 = -1;
		long resettime1 = -1;
		String links = "";
		int lastPageNumber = 1;
		client2 = new DefaultHttpClient();

		HttpGet request1 = new HttpGet(apiURL2);

		request1.addHeader("User-Agent", "Shrey_help");

		HttpResponse response1 = null;
		do {
			if (response1 != null) {
				EntityUtils.consume(response1.getEntity());
			}
			response1 = client2.execute(request1);
			Header[] headers1 = response1.getAllHeaders();

			for (Header header : headers1) {
				if (header.getName().equalsIgnoreCase("X-RateLimit-Remaining")) {
					remaining1 = Integer.parseInt((header.getValue()));
				}
				if (header.getName().equalsIgnoreCase("X-RateLimit-Reset")) {
					resettime1 = Long.parseLong((header.getValue()));
				}
				if (header.getName().equalsIgnoreCase("Link")) {

					links = header.getValue();
				}

			}
			System.out.println("LAST PAGE, Status Code: " + response1.getStatusLine().getStatusCode());
			if (remaining1 == 0) {
				long sleepTimeMiliseconds1 = (resettime1 * 1000 - System.currentTimeMillis()); // extra

				if (sleepTimeMiliseconds1 > 0) {
					try {
						System.out.println("CurrentTime: " + System.currentTimeMillis() + " --Sleeping for: "
								+ (sleepTimeMiliseconds1 + 1000) / 1000 + "seconds --" + "starting again: "
								+ resettime1 * 1000);

						TimeUnit.MILLISECONDS.sleep(sleepTimeMiliseconds1 + 1000);

					} catch (InterruptedException e) {
						System.err.println(e + "--------- Sleep Interrupted");
					}
				}
			}

		} while (response1.getStatusLine().getStatusCode() != 200);

		if (links.length() > 0) {

			String[] splitted = links.split(",");
			if (splitted.length == 2) {

				String lastPageLink = splitted[1].split(";")[0].trim();

				Pattern pattern = Pattern.compile("(page=\\d*)");
				Matcher matcher = pattern.matcher(lastPageLink);

				if (matcher.find()) {
					String a = matcher.group(0).toString().trim();

					String[] test = a.split("=");
					lastPageNumber = Integer.parseInt(test[1]);
				}

			}

		}

		return lastPageNumber;
	}

	public static void checkBadMessage() {
	}

	public static void main(String[] args) throws UnsupportedOperationException, IOException {

		int i = 0;
		while (i < 50000) {
			System.out.println(getLastPage(
					"https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc"));

			i++;
		}

	}
}
