package WriteToFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Project.GithubProject;
import Project.Issue;

public class WritetoFile {
	static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	static FileWriter fw1, fw2;
	static BufferedWriter bw1, bw2;
	static String path = "~/Data/Code/Github/Java/";

	public static void initializeWriting() throws IOException {
		File file1 = new File("./Java_stars_sorted_desc.json");
		File file2 = new File("./shell_script.sh");

		fw1 = new FileWriter(file1.getAbsoluteFile());
		bw1 = new BufferedWriter(fw1);

		fw2 = new FileWriter(file2.getAbsoluteFile());
		bw2 = new BufferedWriter(fw2);
		bw1.write("[");

	}

	public static void writeProjectToFile(GithubProject project) throws IOException {

		/*
		 * Writing to shell script 1. Writing Clone URL inside CODE Directory 2.
		 * Writing All patches and diff in Issues Directory
		 */
		bw2.write("\n\n");

		bw2.write("git clone " + project.getCloneURL() + " " + path + project.getId() + "/" + project.getProjectName()
				+ "/code/");

		bw2.write("\n");

		for (Iterator<Issue> iterator = project.getIsssues().iterator(); iterator.hasNext();) {
			Issue issue = iterator.next();

			if (issue.getPatch().length() != 0) {
				bw2.write("curl -L " + issue.getPatch() + " --create-dirs -o  " + path + project.getId() + "/"
						+ project.getProjectName() + "/Issues/" + issue.getId() + "/"
						+ getFileName(issue.getPatch(), ".patch"));
				bw2.write("\n");

			}

			if (issue.getDiff().length() != 0) {
				bw2.write("curl -L " + issue.getDiff() + " --create-dirs -o  " + path + project.getId() + "/"
						+ project.getProjectName() + "/Issues/" + issue.getId() + "/"
						+ getFileName(issue.getDiff(), ".diff"));
				bw2.write("\n");

			}

		}
		/*
		 * writing json to file
		 */
		bw1.write(gson.toJson(project));
		bw1.write(",");

	}

	public static void finalizeWriting() throws IOException {
		bw1.write("{}");
		bw1.write("]");
		bw1.close();

		bw2.close();

	}

	public static String getFileName(String url, String find) {

		if (url != null) {
			String pattern = "(\\d*" + find + ")";

			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(url);
			if (m.find()) {
				return m.group(0);
			} else {
				return null;
			}
		} else {
			return url;
		}

	}

}
