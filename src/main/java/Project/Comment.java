package Project;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Comment
{

	private Long id;
	private long userId;
	private String date;
	private String body;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@Override
	public String toString()
	{
		return "Comment [id=" + id + ", userId=" + userId + ", date=" + date
				+ ", body=" + body + "]";
	}

	/**
	 * 
	 * @return The id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * 
	 * @return The user
	 */
	public long getUser()
	{
		return userId;
	}

	/**
	 * 
	 * @param user
	 *            The user
	 */
	public void setUser(long user)
	{
		this.userId = user;
	}

	/**
	 * 
	 * @return The date
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * 
	 * @param date
	 *            The date
	 */
	public void setDate(String date)
	{
		this.date = date;
	}

	/**
	 * 
	 * @return The body
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * 
	 * @param body
	 *            The body
	 */
	public void setBody(String body)
	{
		this.body = body;
	}

	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	public Comment(Long id, long userid, String date, String body)
	{
		super();
		this.id = id;
		this.userId = userid;
		this.date = date;
		this.body = body;
	}

}
