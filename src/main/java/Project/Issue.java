package Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Issue
{

	private Long id;
	private String url;
	private String title;
	private String state;
	private String body;
	private String diff;
	private String patch;
	private List<Comment> comments = new ArrayList<Comment>();

	@Override
	public String toString()
	{
		return "Issue [id=" + id + ", url=" + url + ", title=" + title
				+ ", state=" + state + ", body=" + body + ", diff=" + diff
				+ ", patch=" + patch + ", comments=" + comments + "]";
	}

	/**
	 * 
	 * @return The id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * 
	 * @return The url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * 
	 * @param url
	 *            The url
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * 
	 * @return The title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * 
	 * @param title
	 *            The title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * 
	 * @return The state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * 
	 * @param state
	 *            The state
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * 
	 * @return The body
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * 
	 * @param body
	 *            The body
	 */
	public void setBody(String body)
	{
		this.body = body;
	}

	/**
	 * 
	 * @return The diff
	 */
	public String getDiff()
	{
		return diff;
	}

	/**
	 * 
	 * @param diff
	 *            The diff
	 */
	public void setDiff(String diff)
	{
		this.diff = diff;
	}

	/**
	 * 
	 * @return The patch
	 */
	public String getPatch()
	{
		return patch;
	}

	/**
	 * 
	 * @param patch
	 *            The patch
	 */
	public void setPatch(String patch)
	{
		this.patch = patch;
	}

	/**
	 * 
	 * @return The comments
	 */
	public List<Comment> getComments()
	{
		return comments;
	}

	/**
	 * 
	 * @param comments
	 *            The comments
	 */
	public void setComments(List<Comment> comments)
	{
		this.comments = comments;
	}

}
