package Project;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GithubProject
{

	private Long id;
	private long stars;
	private String projectName;

	private String cloneURL;
	private String finalCloneURL;
	private String description;
	private List<Issue> isssues = new ArrayList<Issue>();
	private String homepage;

	@Override
	public String toString()
	{
		return "GithubProject [id=" + id + ", stars=" + stars
				+ ", projectName=" + projectName + ", cloneURL=" + cloneURL
				+ ", finalCloneURL=" + finalCloneURL + ", description="
				+ description + ", isssues=" + isssues + ", homepage="
				+ homepage + "]";
	}

	public long getStars()
	{
		return stars;
	}

	public void setStars(long stars)
	{
		this.stars = stars;
	}

	public String getFinalCloneURL()
	{
		return finalCloneURL;
	}

	public void setFinalCloneURL(String finalCloneURL)
	{
		this.finalCloneURL = finalCloneURL;
	}

	public String getHomepage()
	{
		return homepage;
	}

	public void setHomepage(String homepage)
	{
		this.homepage = homepage;
	}

	/**
	 * 
	 * @return The id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * 
	 * @return The projectName
	 */
	public String getProjectName()
	{
		return projectName;
	}

	/**
	 * 
	 * @param projectName
	 *            The projectName
	 */
	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	/**
	 * 
	 * @return The cloneURL
	 */
	public String getCloneURL()
	{
		return cloneURL;
	}

	/**
	 * 
	 * @param cloneURL
	 *            The cloneURL
	 */
	public void setCloneURL(String cloneURL)
	{
		this.cloneURL = cloneURL;
	}

	/**
	 * 
	 * @return The description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * 
	 * @param description
	 *            The description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * 
	 * @return The isssues
	 */
	public List<Issue> getIsssues()
	{
		return isssues;
	}

	/**
	 * 
	 * @param isssues
	 *            The isssues
	 */
	public void setIsssues(List<Issue> isssues)
	{
		this.isssues = isssues;
	}

}
